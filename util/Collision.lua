local Collision = {}

function Collision.boxCollision(rect1, rect2)
  return rect1.x + rect1.width > rect2.x -- A's right edge is past B's left edge
      and rect1.x < rect2.x + rect2.width -- A's left edge is past B's right edge
      and rect1.y + rect1.height > rect2.y -- A's bottom edge is past B's top edge
      and rect1.y < rect2.y + rect2.height -- A's top edge is past B's bottom edge
end

function Collision.pointCollision(point, rect)
  return point.x > rect.x -- Point is past rect's left edge
      and point.x < rect.x + rect.width -- Point is past rect's right edge
      and point.y > rect.y -- Point is past rect's top edge
      and point.y < rect.y + rect.height -- Point is past rect's bottom edge
end

function Collision.rectFromCenterPoint(obj) 
  return {
    x = obj.x - obj.width/2,
    y = obj.y - obj.height/2,
    width = obj.width,
    height = obj.height
  }
end


return Collision
