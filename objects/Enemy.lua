local Collision = require('util/Collision')

local _speed = 50 
local _pool = {}

Enemy = {}
function Enemy:new(x, y)
  local obj = {}
  setmetatable(obj, self)
  self.__index = self
  
  obj.x = x
  obj.y = y
  obj.width = 20 
  obj.height = 20
  obj.health = 1

  return obj;
end

function Enemy:draw()
  love.graphics.setColor(0, 0, 0, 255)
  love.graphics.rectangle('fill', self.x - self.width/2, self.y - self.width/2, self.width, self.height)
end

function Enemy:update(dt)

end

function Enemy:onHit(bullet)
  self.health = math.max(self.health - bullet.damage, 0)
end

function Enemy:isDead()
  return self.health <= 0
end

function Enemy:getRect()
  return Collision.rectFromCenterPoint(self)
end

function Enemy.get(x, y)
  -- Make a new one if the pool is empty
  if table.getn(_pool) == 0 then
    return Enemy:new(x, y)
  end

  -- Otherwise grab one from the pool and updates it's properties
  local enemy = table.remove(_pool, 1)
  enemy.x = x
  enemy.y = y
  return enemy
end

function Enemy.recycle(enemy)
  table.insert(_pool, enemy)
end
