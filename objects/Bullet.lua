local Collision = require('util/Collision')

local _speed = 800
local _pool = {}

Bullet = {}
function Bullet:new(x, y)
  local obj = {}
  setmetatable(obj, self)
  self.__index = self
  
  obj.x = x
  obj.y = y
  obj.width = 5 
  obj.height = 5
  obj.damage = 1

  return obj;
end

function Bullet:draw()
  love.graphics.setColor(0, 0, 0, 255)
  love.graphics.rectangle('fill', self.x - self.width/2, self.y - self.width/2, self.width, self.height)
end

function Bullet:update(dt)
  self.y = self.y - _speed * dt
end

function Bullet:getRect()
  return Collision.rectFromCenterPoint(self)
end

function Bullet.get(x, y)
  -- Make a new one if the pool is empty
  if table.getn(_pool) == 0 then
    return Bullet:new(x, y)
  end

  -- Otherwise grab one from the pool and updates it's properties
  local bullet = table.remove(_pool, 1)
  bullet.x = x
  bullet.y = y
  return bullet

end

function Bullet.recycle(bullet)
  table.insert(_pool, bullet)
end
