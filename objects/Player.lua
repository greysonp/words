local Collision = require('util/Collision')

local speed = 300

Player = {}
Player.__index = Player
function Player:new(x, y)
  local obj = {}
  setmetatable(obj, Player)
  self.__index = self

  obj.x = x 
  obj.y = y 
  obj.width = 20
  obj.height = 30

  return obj
end

function Player:draw()
  love.graphics.setColor(0, 0, 0, 255)
  love.graphics.rectangle('fill', self.x - self.width/2, self.y - self.height/2, self.width, self.height)
end

function Player:update(dt)
  if love.keyboard.isDown('w') then
    self.y = self.y - speed * dt
  end
  if love.keyboard.isDown('s') then
    self.y = self.y + speed * dt
  end
  if love.keyboard.isDown('a') then
    self.x = self.x - speed * dt
  end
  if love.keyboard.isDown('d') then
    self.x = self.x + speed * dt
  end
end

function Player:getRect()
  return Collision.rectFromCenterPoint(self)
end
