local Manifest = require('Manifest')

local _speed = 100 
local _pool = {}

Block = {}
function Block:new(x, y)
  local obj = {}
  setmetatable(obj, self)
  self.__index = self
 
  -- We size the blocks so we can fit exactly the right number per row
  local size = Manifest.width / Manifest.columns
  obj.width = size
  obj.height = size

  -- Round to nearest column
  obj.x = math.floor(x / obj.width) * obj.width
  obj.y = y
  obj.damage = 1
  obj.stopped = false

  return obj;
end

function Block:draw()
  love.graphics.setColor(0, 0, 0, 255)
  love.graphics.rectangle('fill', self.x, self.y, self.width, self.height)
end

function Block:update(dt)
  if not self.stopped then
    self.y = self.y + _speed * dt
  end
end

function Block:stop()
  self.stopped = true
end

function Block:isStopped()
  return self.stopped
end

function Block:getRect()
  return self
end

function Block.get(x, y)
  -- Make a new one if the pool is empty
  if table.getn(_pool) == 0 then
    return Block:new(x, y)
  end

  -- Otherwise grab one from the pool and updates it's properties
  local block = table.remove(_pool, 1)
  block.x = math.floor(x / block.width) * block.width
  block.y = y
  block.stopped = false
  return block 

end

function Block.recycle(block)
  table.insert(_pool, block)
end
