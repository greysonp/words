require('objects/Player')
require('objects/Bullet')
require('scenes/GameScene')

local Manifest = require('Manifest')
local _scene = nil

function love.load()
  love.window.setMode(Manifest.width, Manifest.height, { centered = true })
  _scene = GameScene:new()
  _scene:load()
end

function love.draw()
  love.graphics.clear(255, 255, 255, 255)
  _scene:draw()
end

function love.update(dt)
  _scene:update(dt)
end

function love.keypressed(key)
  -- Debug
  if key == 'rctrl' then
    debug.debug()
  end
end

