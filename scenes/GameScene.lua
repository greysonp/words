require('scenes/Scene')
require('objects/Player')
require('objects/Bullet')
require('objects/Enemy')
require('objects/Block')

local MIN_TIME_BETWEEN_BULLETS = 1.0/5

local Manifest = require('Manifest')
local Collision = require('util/Collision')
local _player = nil 
local _bullets = {}
local _enemies = {}
local _blocks = {}
local _timeBetweenBullets = 0

GameScene = Scene:new() 

function GameScene:load()
  Scene.load(self)

  -- Add player
  _player = Player:new(Manifest.width/2, Manifest.height - 100)
  self:addChild(_player)

  -- Add some test enemies
  for i = 0, Manifest.columns - 1, 1 do -- Start at 0, so don't need to do last one
    local enemy = Enemy:new(i * (Manifest.width / Manifest.columns), 100)
    enemy.x = enemy.x + enemy.width / 2
    table.insert(_enemies, enemy)
    self:addChild(enemy)
  end
end

function GameScene:update(dt)
  Scene.update(self, dt)

  -- Handle shooting
  _timeBetweenBullets = math.min(_timeBetweenBullets + dt, MIN_TIME_BETWEEN_BULLETS)
  if _timeBetweenBullets == MIN_TIME_BETWEEN_BULLETS and love.keyboard.isDown('space') then
    local bullet = Bullet.get(_player.x, _player.y - _player.height/2)
    self:addChild(bullet)
    table.insert(_bullets, bullet)
    _timeBetweenBullets = 0
  end

  -- Handle bullets going off-screen
  for i = #_bullets, 1, -1 do
    local bullet = _bullets[i]
    if bullet.y < bullet.width then 
      self:removeBullet(bullet, i)
    end
  end

  -- Collision detection for enemies 
  for i, enemy in pairs(_enemies) do
    local enemyRect = enemy:getRect() 

    -- Collision with bullets
    for j, bullet in pairs(_bullets) do
      if Collision.boxCollision(enemyRect, bullet:getRect()) then
        enemy:onHit(bullet)
        self:removeBullet(bullet, j)
        if enemy:isDead() then
          self:spawnBlock(enemy.x, enemy.y)
        end
      end
    end

    -- Collision with player
    if Collision.boxCollision(enemyRect, _player:getRect()) then
      print('Player hit enemy!')
    end
  end

  -- Handle blocks stacking
  for i, block in pairs(_blocks) do
    -- Cache block's rect
    local blockRect = block:getRect()

    -- Only do inter-block collisions on moving blocks
    if not block:isStopped() then
      -- Stop blocks when they hit the bottom
      if block.y + block.height >= Manifest.height then
        block.y = Manifest.height - block.height
        block:stop()
        self:checkForCompletedRow()
      end

      -- Stop blocks when they hit each other
      for j, block2 in pairs(_blocks) do
        if block ~= block2 and Collision.boxCollision(blockRect, block2:getRect()) then
          block.y = block2.y - block.height
          block:stop()
          self:checkForCompletedRow()
        end
      end
    end

    -- Collision with player
    if Collision.boxCollision(blockRect, _player:getRect()) then
      print('Player hit block')
    end
  end
end

function GameScene:spawnBlock(x, y)
  local block = Block.get(x, y)
  self:addChild(block)
  table.insert(_blocks, block)
end

function GameScene:removeBullet(bullet, i)
  table.remove(_bullets, i)
  self:removeChild(bullet)
  Bullet.recycle(bullet)
end

function GameScene:checkForCompletedRow()
  local py = Manifest.height - 1
  local columnWidth = Manifest.width / Manifest.columns

  -- Go through each column
  for i = 0, Manifest.columns - 1, 1 do -- Start at 0, so don't need to look at last index 
    local px = columnWidth * i + (columnWidth / 2)
    if not self:isOccupied({x = px, y = py}) then
      return
    end
  end

  -- If we made it through all of the columns, then we know we have a completed row
  self:rowCompleted()
end

function GameScene:isOccupied(point)
  -- Dear god come up with something better than this
  for i, block in pairs(_blocks) do
    if Collision.pointCollision(point, block:getRect()) then
      return true
    end
  end
end

function GameScene:rowCompleted()
  -- TODO: Switch to a printer call
  os.execute('echo "row completed"')

  -- Go through all stopped blocks and shift them down
  for i = #_blocks, 1, -1 do
    local block = _blocks[i]
    if block:isStopped() then
      block.y = block.y + block.height

      -- If a block was shifted off-screen, remove it
      if block.y >= Manifest.height then
        self:removeChild(block)
        table.remove(_blocks, i)
        Block.recycle(block)
      end
    end
  end
end
