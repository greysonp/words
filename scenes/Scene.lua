Scene = {}
function Scene:new()
  local obj = {}
  setmetatable(obj, self)
  self.__index = self

  obj.children = {}

  return obj 
end

function Scene:load()
end

function Scene:update(dt)
  for i, child in pairs(self.children) do
    child:update(dt)
  end
end

function Scene:draw(dt)
  for i, child in pairs(self.children) do
    child:draw()
  end
end

function Scene:addChild(child)
  table.insert(self.children, child)
end

function Scene:removeChild(childToRemove)
  for i, child in pairs(self.children) do
    if child == childToRemove then 
      table.remove(self.children, i)
      break
    end
  end
end

